const functions = require('firebase-functions');


const admin = require('firebase-admin');
admin.initializeApp();

exports.addMessage = functions.https.onRequest((req, res) => {
  const original = req.query.text;
  return admin.database().ref('/messages').push({original: original}).then((snapshot) => {
    return res.redirect(303, snapshot.ref.toString());
  });
});


exports.makeUppercase = functions.database.ref('/messages/{pushId}/original')
  .onCreate((snapshot, context) => {
    const original = snapshot.val();
    console.log('Uppercasing', context.params.pushId, original);
    const uppercase = original.toUpperCase();
    return snapshot.ref.parent.child('uppercase').set(uppercase);
  });

exports.createNote = functions.firestore.document('/notes/{noteId}/name')
  .onCreate((snapshot, context) => {

    const newValue = snapshot.data();
    const name = newValue.name;
    // context.params.userId;
    // const uppercase = name.toUpperCase();
    return snapshot.ref.parent.doc('address').set('0x2222222PPSX');
  });


exports.createUser = functions.firestore.document('/users/{userId}')
  .onCreate((snapshot, context) => {

    const newValue = snapshot.data();
    const name = newValue.name;
    //context.params.userId;
    const uppercase = name.toUpperCase();
    //return snapshot.ref.parent.add('uppercase').set(uppercase);
    return snapshot.ref.parent.add('uppercase').set(uppercase);
  });
