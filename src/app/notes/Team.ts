export interface Team {
  name: string;
  address: string;
}

export function createEmptyTeam(): Team {
  return {
    name: '',
    address: ''
  };
}
