// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  apiKey: 'AIzaSyAKaISebngN42b0aLTsNVi_TI8Ui1rK7lw',
  authDomain: 'ace-computer-126507.firebaseapp.com',
  databaseURL: 'https://ace-computer-126507.firebaseio.com',
  projectId: 'ace-computer-126507',
  storageBucket: 'ace-computer-126507.appspot.com',
  messagingSenderId: '418736758167',
  production: true
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
